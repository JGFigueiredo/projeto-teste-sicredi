# Prova técnica API

Desafio Prova tecnica Sicredi.

##  Requisitos
 * Java 16+ JDK deve estar instalado
 * Maven
 * Junit 4.12
 * Restassured 4.5.1
 * Wiremock
 
## Como executar a aplicação 

Utilizando a ferramente Maven, na parte lateral à direita clicando no simbolo do Maven a letra M o mesmo aparece opções para executar, atraves de uma caixa de pesquisa, no qual pesquisaremos pelo nome:

```bash
mvn clean spring-boot:run
```

## Documentacão técnica da aplicação

A documentação técnica da API está disponível através do OpenAPI/Swagger em [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

## Regras

### Restrições

`GET <host>/api/v1/restricoes/{cpf}`

O endpoint que excurar conforme os cpfs com restrições abaixo, consultara o mesmo e retornará se possuirá ou não restrição. 

* Se não possui restrição do HTTP Status 204 é retornado
* Se possui restrição o HTTP Status 200 é retornado com a mensagem "O CPF 99999999999 possui restrição"

#### CPFs com restrição

| CPF |
| ----|
| 97093236014 |
| 60094146012 |
| 84809766080 |
| 62648716050 |
| 26276298085 |
| 01317496094 |
| 55856777050 |
| 19626829001 |
| 24094592008 |
| 58063164083 |

### Simulações

A simulação é um cadastro que ficará registrado informações importantes sobre o crédito como valor, parcelas, 
dados de contato, etc...

### Criar uma simulação

`POST <host>/api/v1/simulacoes`

Endpoint é responsável por inserir uma nova simulação.

Informções, com suas regras:

| Atributo | Obrigatório? | Regra |
|----------|--------------|-------|
| cpf | sim | texto informando o CPF não no formato 999.999.999-99 |
| nome | sim | texto informando o nome da pessoa |
| email | sim | texto informado um e-mail válido |
| valor | sim | valor da simulação que deve ser igual ou maior que R$ 1.000 e menor ou igual que R$ 40.000 |
| parcela | sim | número de parcelas para pagamento que deve ser igual ou maior que 2 e menor ou igual a 48 |
| seguro | sim | booleano `true` se com seguro e  `false` se sem seguro |

* Uma simulação cadastrada com sucesso retorna o HTTP Status 201 e os dados inseridos como retorno
* Uma simulação com problema em alguma regra retorna o HTTP Status 400 com a lista de erros
* Uma simulação para um mesmo CPF retorna um HTTP Status 409 com a mensagem "CPF já existente"

### Alterar uma simulação

`PUT <host>/api/v1/simulacoes/{cpf}`

Irá alterar a simulação que já existe, no qual o CPF deve ser informado para que a alteração possa ser efetuada.

* A alteração pode ser feita em qualquer atributo da simulação
* As mesmas regras se mantém
* Se o CPF não possuir uma simulação o HTTP Status 404 é retornado com a mensagem "CPF não encontrado"

### Consultar todas a simulações cadastradas

`GET <host>/api/v1/simulacoes`

Listas cadastradas.

* Retorna a lista de simulações cadastradas e existir uma ou mais
* Retorna HTTP Status 204 se não existir simulações cadastradas


### Consultar uma simulação pelo CPF

`GET <host>/api/v1/simulacoes/{cpf}`

Irá retornar a simulação cadastrada pelo CPF.

* Retorna a simulação cadastrada
* Se o CPF não possuir uma simulação o HTTP Status 404 é retornado

### Remover uma simulação

`DELETE <host>/api/v1/simulacoes/{id}`

Removerá uma das simulações cadastrada pelo seu ID.

* Retorna o HTTP Status 204 se simulação for removida com sucesso
* Retorna o HTTP Status 404 com a mensagem "Simulação não encontrada" se não existir a simulação pelo ID informado

