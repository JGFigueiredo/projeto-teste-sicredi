package br.com.sicredi.simulacao;

import io.restassured.http.ContentType;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import static io.restassured.RestAssured.*;

//Verifica se CPF possui restrições
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RestricaoTest extends ConfiguracoesAPI{

    //Cenário 1
    @Test
    public void testeCpfComRestricaoStatus200(){

        String cpfParametro = "84809766080";

        //Definindo parâmetro com CPF que possui restrições
        given()
                .contentType(ContentType.JSON)
                .pathParam("cpf", cpfParametro).

                when()
                .get("/v1/restricoes/{cpf}").

                then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void testeCpfSemRestricaoStatus204(){

        String cpfParametro = "17822386034";

        //Definindo parâmetro um CPF que não possui restrições
        given()
                .contentType(ContentType.JSON)
                .pathParam("cpf", cpfParametro).

                when()
                .get("/v1/restricoes/{cpf}").

                then()
                .assertThat()
                .statusCode(204);

    }
}
