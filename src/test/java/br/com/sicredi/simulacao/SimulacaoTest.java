package br.com.sicredi.simulacao;

import br.com.sicredi.dto.SimulacaoDTO;
import io.restassured.http.ContentType;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import java.math.BigDecimal;
import static io.restassured.RestAssured.*;

//Criação dos métodos para efetuação dos testes

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SimulacaoTest extends ConfiguracoesAPI {

    //Enviando e retornando o Status do código 200
    @Test
    public void testeParaRetonarTodosInformacoesEStatus200() {

        //Dado que eu defino o parâmetro do header content type como "application/json"
        given()
                .contentType(ContentType.JSON).

                when()
                .get("/v1/simulacoes").

                then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testeComPreenchimentoDeTodasInformacoesEStatus201() {

        String nome = "Tarcio Almeida";
        String cpf = "21548643092";
        String email = "email01@email.com";
        BigDecimal valor = BigDecimal.valueOf(2000);
        int parcelas = 3;
        boolean seguro = true;
        SimulacaoDTO simulacaoDTO = new SimulacaoDTO(nome, cpf, email, valor, parcelas, seguro);

        // E defino o body com nome, cpf, email, valor, parcelas e seguro
        given()
                .contentType(ContentType.JSON)
                .body(simulacaoDTO.converterParaJson()).

                when()
                .post("/v1/simulacoes").

                then()
                .assertThat()
                .statusCode(201);
    }

    @Test
    public void testeComInformacoesIncompletasEStatus400() {

        String nome = "Tarcio Almeida 02";
        String cpf = "19080661058";
        String email = null;
        BigDecimal valor = BigDecimal.valueOf(2000);
        int parcelas = 3;
        boolean seguro = true;
        SimulacaoDTO simulacaoDTO = new SimulacaoDTO(nome, cpf, email, valor, parcelas, seguro);

        // Definindo com todas as informações preenchidas
        given()
                .contentType(ContentType.JSON)
                .body(simulacaoDTO.converterParaJson()).

                when()
                .post("/v1/simulacoes").

                then()
                .assertThat()
                .statusCode(400);
    }

    @Test
    public void testeComCPFExistenteEStatus409() {

        String nome = "Tamires Oliveira";
        String cpf = "17822386034";
        String email = "teste@email.com";
        BigDecimal valor = BigDecimal.valueOf(30000);
        int parcelas = 12;
        boolean seguro = false;
        SimulacaoDTO simulacaoDTO = new SimulacaoDTO(nome, cpf, email, valor, parcelas, seguro);

        //Definindo com as informçãoes preenchidas e validando CPF's já existentes
        given()
                .contentType(ContentType.JSON)
                .body(simulacaoDTO.converterParaJson()).

                when()
                .post("/v1/simulacoes").

                then()
                .assertThat()
                .statusCode(409);
    }

    @Test
    public void testeDeCpfComSinulacoesJaExistentesStatus200() {

        String cpfParametro = "17822386034";

        //Definindo CPF com sumilação que já existe
        given()
                .contentType(ContentType.JSON)
                .pathParam("cpf", cpfParametro).

                when()
                .get("/v1/simulacoes/{cpf}").

                then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testeDeCpfNaoAssociadoComSinulacoesJaExistentesStatus404() {

        String cpfParametro = "12345678900";

        //Definindo CPF com sumilação que já não existe
        given()
                .contentType(ContentType.JSON)
                .pathParam("cpf", cpfParametro).

                when()
                .get("/v1/simulacoes/{cpf}").

                then()
                .assertThat()
                .statusCode(404);
    }

    @Test
    public void testeDeCpfAssociadoASimulacaoExistenteStatus200() {

        String nome = "Tarcio Almeida 04";
        String cpf = null;
        String cpfParametro = "66414919004";
        String email = "email04@email.com";
        BigDecimal valor = BigDecimal.valueOf(2000);
        int parcelas = 40;
        boolean seguro = true;
        SimulacaoDTO simulacaoDTO = new SimulacaoDTO(nome, cpf, email, valor, parcelas, seguro);

        //Definindo CPF associado e com simulaçõa existente e com informações a serem alteradas
        given()
                .contentType(ContentType.JSON)
                .pathParam("cpf", cpfParametro)
                .body(simulacaoDTO.converterParaJson()).

                when()
                .put("/v1/simulacoes/{cpf}").

                then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testeDeCpfNaoAssociadoASimulacaoExistenteStatus404() {

        String nome = "Tarcio Almeida 05";
        String cpf = null;
        String cpfParametro = "12345678900";
        String email = "email05@email.com";
        BigDecimal valor = BigDecimal.valueOf(10000);
        int parcelas = 36;
        boolean seguro = true;
        SimulacaoDTO simulacaoDTO = new SimulacaoDTO(nome, cpf, email, valor, parcelas, seguro);

        //Definindo CPF não associado e com simulaçõa existente e com informações a serem alteradas
        given()
                .contentType(ContentType.JSON)
                .pathParam("cpf", cpfParametro)
                .body(simulacaoDTO.converterParaJson()).

                when()
                .put("/v1/simulacoes/{cpf}").

                then()
                .assertThat()
                .statusCode(404);
    }


    @Test
    public void testeDeletandoAPartirDoID() {

        String idParametro = "11";

        //Definindo o Identificador associado e com simulaçõa existente
        given()
                .contentType(ContentType.JSON)
                .pathParam("id", idParametro).

                when()
                .delete("/v1/simulacoes/{id}").

                then()
                .assertThat()
                .statusCode(200);
    }
}
