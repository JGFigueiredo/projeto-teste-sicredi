package br.com.sicredi.simulacao;

import io.restassured.RestAssured;
import org.junit.BeforeClass;

public abstract class ConfiguracoesAPI {

    @BeforeClass
    public static void definicaoAPIURL() {
        //Definição da URL REST API
        RestAssured.baseURI = "http://localhost/";
        RestAssured.port = 8080;
        RestAssured.basePath = "/api";
    }

}



